﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Enemy_Controller : Character_Controller
{
    static int score = 0;

    Transform target = Player_Controller.trans;

    NavMeshAgent agent;

    // Start is called before the first frame update
    override protected void Start()
    {
        base.Start();
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    override protected void Update()
    {
        base.Update();
        agent.destination = target.position;
    }

    override protected void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);
        void Die()
        {
            score++;
            animator.enabled = false;
        }

        if(collision.gameObject.CompareTag("Projectile"))
        {
            Die();
        }
    }
}
