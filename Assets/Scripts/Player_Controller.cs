﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : Character_Controller
{
    public static Transform trans;

    new Rigidbody rigidbody;
    Camera head;
    LineRenderer lineRenderer;
    [SerializeField] Transform grapplingHookStart;

    // Start is called before the first frame update
    override protected void Start()
    {
        base.Start();
        rigidbody = GetComponent<Rigidbody>();
        head = GetComponentInChildren<Camera>();
        Cursor.lockState = CursorLockMode.Locked;
        lineRenderer = GetComponent<LineRenderer>();
        Debug.Log("player");
        trans = transform;
    }

    float rotationY = 0f, rotationX = 0f;
    Ray headRay { get => head.ScreenPointToRay(new Vector3(head.pixelWidth - 1, head.pixelHeight - 1) / 2); }
    private void LateUpdate()
    {
        rotationY += Input.GetAxis("Mouse Y"); rotationX += Input.GetAxis("Mouse X");
        if (isGrounded)
        {
            if (rotationY > 80) rotationY = 80;
            else if (rotationY < -45) rotationY = -45;
        }
        transform.rotation = Quaternion.Euler(0, rotationX, 0);
        animator.GetBoneTransform(HumanBodyBones.Head).Rotate(new Vector3(rotationY, 0, 0), Space.Self);
        if (grappling) ;
        else if (Physics.Raycast(headRay, out hit, maxDistance: float.MaxValue)) gun.transform.LookAt(hit.point);
    }

    float grapplingSpeed = 30f;
    bool grappling = false;
    float contactSpeed = 3f;
    IEnumerator GrappleHook()
    {
        float contact = 0f;
        bool contacted = false;
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = 2;
        grappling = true;
        gun.lookAtPosition = hit.point;
        while (Input.GetKey(KeyCode.Mouse1))
        {
            lineRenderer.SetPosition(0, grapplingHookStart.position);
            if (contacted)
            {
                rigidbody.AddForce((hit.point - transform.position).normalized * grapplingSpeed, ForceMode.Acceleration);
            }
            else
            {
                lineRenderer.SetPosition(1, Vector3.Lerp(transform.position, hit.point, contact += 2 * contactSpeed * Time.deltaTime));
                contacted = contact > 1;
            }

            yield return new WaitForEndOfFrame();
        }
        grappling = false;
        lineRenderer.positionCount = 0;
    }

    override protected void FixedUpdate()
    {
        base.FixedUpdate();

        Vector3 move()
        {
            Vector3 r = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized;
            r = Quaternion.AngleAxis(rigidbody.rotation.eulerAngles.y, Vector3.up) * r;
            animator.SetBool("Walk", r.magnitude > 0);
            animator.SetBool("Run", Input.GetKey(KeyCode.LeftShift));
            r *= (animator.GetBool("Run") ? runSpeed : walkSpeed);
            r.y = rigidbody.velocity.y;
            return r;
        }

        if (isGrounded)
        {
            rigidbody.velocity = move();
            rigidbody.AddForce(Input.GetKeyUp(KeyCode.Space) ? Vector3.up * 10 : Vector3.zero, ForceMode.VelocityChange);

        }
    }

    override protected void Update()
    {
        base.Update();
        if (Input.GetKey(KeyCode.Mouse1) && !grappling && gun.Hostered) StartCoroutine(GrappleHook());
    }
}
