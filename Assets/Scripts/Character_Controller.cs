﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(LineRenderer))]
public class Character_Controller : MonoBehaviour
{
    protected bool isGrounded = false, OnWall = false;
    
    [SerializeField] protected static float walkSpeed = 1f, runSpeed = 4f;
    

    [SerializeField] protected Gun_Controller gun;

    protected Animator animator;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        animator = GetComponent<Animator>();
        Debug.Log("Character");
    }

    
    protected RaycastHit hit;
    protected virtual void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            animator.SetTrigger("Hostel Pistol");
            gun.Hostered = !gun.Hostered;
        }
    }

    //[SerializeField] float upForce = 1f;
    //bool WallRunning = false;
    //IEnumerator WallRun()
    //{
    //    WallRunning = true;
    //    rigidbody.useGravity = false;
    //    while(OnWall)
    //    {
    //        rigidbody.AddForce(Vector3.up * upForce);
    //        yield return new WaitForEndOfFrame();
    //    }
    //    rigidbody.AddRelativeForce(Vector3.up, ForceMode.VelocityChange);
    //    rigidbody.useGravity = true;
    //    WallRunning = false;
    //}

    
    // Update is called once per frame
    protected virtual void FixedUpdate()
    {
        //if (OnWall && !WallRunning) StartCoroutine(WallRun());

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            gun.shoot = true;
            animator.SetTrigger("Shoot");
        }
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor")) isGrounded = true;
        else if (collision.gameObject.CompareTag("Wall"))
        {
            OnWall = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor")) isGrounded = false;
        else if (collision.gameObject.CompareTag("Wall"))
        {
            OnWall = false;
        }
    }
}