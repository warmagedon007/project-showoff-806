﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gun_Controller : MonoBehaviour
{
    public bool Hostered = false;

    public Vector3 lookAtPosition = Vector3.zero;

    [SerializeField] GameObject bullet;

    [SerializeField] Transform barrel;

    public bool shoot;

    Slider[] sliders;

    float count = 0f;

    IEnumerator Shoot()
    {
        while(true)
        {
            if(shoot && count > 3 && Hostered)
            {
                var b = Instantiate(bullet, barrel.position, barrel.rotation);
                b.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * 100, ForceMode.VelocityChange);
                
                count = 0f;
            }
            shoot = false;
            count += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
    private void Start()
    {
        sliders = GetComponentsInChildren<Slider>();
        StartCoroutine(Shoot());
        foreach (var item in sliders)
        {
            item.maxValue = 3f;
            item.minValue = 0f;
        }
    }

    void Update()
    {
        foreach (var item in sliders)
        {
            item.value = count;
        }
    }
}
