﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] GameObject Object;

    IEnumerator spawn(float interval)
    {
        float count = interval;
        while(true)
        {
            if(count > interval)
            {
                count = 0;
                Instantiate(Object, transform.position, Quaternion.identity);
            }
            count += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawn(Random.Range(40f, 60f)));
    }

}
